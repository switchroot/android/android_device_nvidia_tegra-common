LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.comms.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.comms.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.data_bin.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.data_bin.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.gps.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.gps.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.hdcp.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.hdcp.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.lkm.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.lkm.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.none.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.none.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.tegra.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.tegra.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.tegra_emmc.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.tegra_emmc.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.tegra_sata.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.tegra_sata.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.tegra_sd.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.tegra_sd.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.tf.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.tf.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.tlk.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.tlk.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.xusb.configfs.usb.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.xusb.configfs.usb.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.nv_dev_board.usb.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.nv_dev_board.usb.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.sata.configs.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := init.sata.configs.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init/hw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE       := init.recovery.xusb.configfs.usb.rc
LOCAL_MODULE_CLASS := ETC
LOCAL_SRC_FILES    := init.xusb.configfs.usb.rc
LOCAL_MODULE_PATH  := $(TARGET_ROOT_OUT)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE       := init.recovery.nv_dev_board.usb.rc
LOCAL_MODULE_CLASS := ETC
LOCAL_SRC_FILES    := init.nv_dev_board.usb.rc
LOCAL_MODULE_PATH  := $(TARGET_ROOT_OUT)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := adbenable
LOCAL_SRC_FILES     := adbenable.sh
LOCAL_MODULE_SUFFIX := .sh
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_OWNER  := nvidia
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := wifi_loader
LOCAL_SRC_FILES     := wifi_loader.sh
LOCAL_MODULE_SUFFIX := .sh
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_OWNER  := nvidia
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := bt_loader
LOCAL_SRC_FILES     := bt_loader.sh
LOCAL_MODULE_SUFFIX := .sh
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_OWNER  := nvidia
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := wireguard.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := wireguard.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := config_cameras
LOCAL_SRC_FILES     := config_cameras.sh
LOCAL_MODULE_SUFFIX := .sh
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_OWNER  := nvidia
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := tegra_camera.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_SRC_FILES            := tegra_camera.rc
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init
include $(BUILD_PREBUILT)

# Camera permission symlinks
ifeq ($(TARGET_TEGRA_CAMERA),nvcamera)
DATA_ETC_PATH   := /data/vendor/camera_config/etc
VENDOR_ETC_PATH := $(TARGET_OUT_VENDOR)/etc

CAMERA_SYMLINKS := \
    android.hardware.camera.autofocus.xml \
    android.hardware.camera.external.xml \
    android.hardware.camera.flash-autofocus.xml \
    android.hardware.camera.front.xml \
    android.hardware.camera.full.xml \
    android.hardware.camera.manual_sensor.xml \
    android.hardware.camera.manual_postprocessing.xml \
    android.hardware.camera.raw.xml \
    android.hardware.camera.xml

INSTALLED_CAMERA_SYMLINKS := $(CAMERA_SYMLINKS:%=$(VENDOR_ETC_PATH)/permissions/%)
$(INSTALLED_CAMERA_SYMLINKS): $(LOCAL_INSTALLED_MODULE)
	$(hide) ln -sf $(DATA_ETC_PATH)/permissions/$(notdir $@) $@

PROFILE_SYMLINKS := \
    media_profiles_V1_0.xml

INSTALLED_PROFILE_SYMLINKS := $(PROFILE_SYMLINKS:%=$(VENDOR_ETC_PATH)/%)
$(INSTALLED_PROFILE_SYMLINKS): $(LOCAL_INSTALLED_MODULE)
	$(hide) ln -sf $(DATA_ETC_PATH)/$(notdir $@) $@

ALL_DEFAULT_INSTALLED_MODULES += $(INSTALLED_CAMERA_SYMLINKS) $(INSTALLED_PROFILE_SYMLINKS)
endif
